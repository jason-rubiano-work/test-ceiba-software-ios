//
//  PostCard.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import SwiftUI

struct PostCard: View {
    
    var post: PostsRealm
    
    var body: some View {
        
        VStack(alignment: .leading, spacing: 6) {
            
            Text(post.title ?? "")
                .font(.system(size: 18, weight: .bold))
                .foregroundColor(Color.green)
                .multilineTextAlignment(.leading)
                .fixedSize(horizontal: false, vertical: true)
            
            Text(post.body ?? "")
                .font(.system(size: 16, weight: .medium))
                .multilineTextAlignment(.leading)
                .fixedSize(horizontal: false, vertical: true)
            
        }.padding(.horizontal, 16)
            .padding(.vertical, 10)
            .frame(maxWidth: .infinity, alignment: .leading)
            .overlay(
                RoundedRectangle(cornerRadius: 8)
                    .stroke(Color.gray, lineWidth: 2)
            )
    }
}

struct PostCard_Previews: PreviewProvider {
    static var previews: some View {
        PostCard(post: PostsRealm())
    }
}
