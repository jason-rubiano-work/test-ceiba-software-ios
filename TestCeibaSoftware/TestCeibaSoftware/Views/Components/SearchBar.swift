//
//  SearchBar.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import SwiftUI

struct SearchBar: View {
    
    @State private var shouldAnimate = false
    
    @Binding var searhText: String
    @State var isSearching: Bool = false
    
    var body: some View {
        
        HStack(spacing: 0) {
            
            HStack {
                
                TextField("Buscar...", text: $searhText)
                    .padding(.leading, 24)
                
            }.padding()
                .background(Color(.systemGray5))
                .cornerRadius(6)
                .padding(.horizontal)
                .onTapGesture {
                    withAnimation(.spring()) {
                        self.isSearching = true
                    }
                }
                .overlay(
                    
                    HStack {
                        
                        Image(systemName: "magnifyingglass")
                        
                        Spacer()
                        
                        if isSearching {
                            
                            Button(action: {
                                
                                searhText = ""
                            }) {
                                
                                Image(systemName: "xmark.circle.fill")
                                    .padding(.vertical)
                            }
                        }
                        
                    }.padding(.horizontal, 32)
                        .foregroundColor(.gray)
                )
            
            if isSearching {
                
                Button(action: {
                    
                    withAnimation(.spring()) {
                        self.isSearching = false
                    }
                    searhText = ""
                    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                    
                }) {
                    
                    Text("cancel")
                        .padding(.trailing)
                        .padding(.leading, 0)
                }
            }
        }
    }
}

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchBar(searhText: .constant(""))
    }
}
