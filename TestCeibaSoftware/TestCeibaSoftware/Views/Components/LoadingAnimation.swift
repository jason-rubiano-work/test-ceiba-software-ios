//
//  LoadingAnimation.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import SwiftUI

struct LoadingAnimation: View {
    
    @State private var shouldAnimate = false
    
    var body: some View {
        
        VStack(spacing: 20) {
            
            Text("Loading...")
                .font(.system(.body, design: .rounded))
                .bold()
            
            HStack {
                
                Circle()
                    .fill(Color.blue)
                    .frame(width: 20, height: 20)
                    .scaleEffect(shouldAnimate ? 1.0 : 0.5)
                    .animation(Animation.easeInOut(duration: 0.5).repeatForever(), value: self.shouldAnimate)
                
                Circle()
                    .fill(Color.blue)
                    .frame(width: 20, height: 20)
                    .scaleEffect(shouldAnimate ? 1.0 : 0.5)
                    .animation(Animation.easeInOut(duration: 0.5).repeatForever().delay(0.3), value: self.shouldAnimate)
                
                Circle()
                    .fill(Color.blue)
                    .frame(width: 20, height: 20)
                    .scaleEffect(shouldAnimate ? 1.0 : 0.5)
                    .animation(Animation.easeInOut(duration: 0.5).repeatForever().delay(0.6), value: self.shouldAnimate)
                
            }.onAppear {
                DispatchQueue.main.async {
                    self.shouldAnimate = true
                }
            }
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingAnimation()
    }
}
