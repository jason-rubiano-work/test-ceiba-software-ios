//
//  UserCard.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import SwiftUI

struct UserCard: View {
    
    var user: UserRealm
    @State var modalPosts: Bool = false
    
    var body: some View {
        
        VStack(spacing: 10) {
            
            VStack(alignment: .leading, spacing: 5) {
             
                Text(user.name ?? "")
                    .font(.system(size: 18, weight: .bold))
                    .foregroundColor(Color.green)
                
                HStack(spacing: 6) {
                    
                    Image(systemName: "phone.fill")
                        .foregroundColor(.green)
                        .imageScale(.medium)
                    
                    Text(user.phone ?? "")
                        .font(.system(size: 16, weight: .medium))
                }
                
                HStack(spacing: 6) {
                    
                    Image(systemName: "envelope.fill")
                        .foregroundColor(.green)
                        .imageScale(.medium)
                    
                    Text(user.email ?? "")
                        .font(.system(size: 16, weight: .medium))
                }
                
            }.frame(maxWidth: .infinity, alignment: .leading)
            
            HStack(spacing: 0) {
                
                Spacer()
                
                Button(action: {
                    
                    modalPosts.toggle()
                    
                }) {
                    
                    Text("VER PUBLICACIONES")
                        .font(.system(size: 16, weight: .medium))
                }
            }
            
        }.padding(.horizontal, 16)
            .padding(.vertical, 10)
            .background(Color.white)
            .cornerRadius(8)
            .shadow(color: Color.black.opacity(0.2), radius: 5)
            .sheet(isPresented: $modalPosts) {
                PostsDetailsView(modal: $modalPosts, userId: user.id, name: user.name ?? "", email: user.email ?? "", phone: user.phone ?? "")
                    .environmentObject(RealmObserver())
            }
    }
}

struct UserCard_Previews: PreviewProvider {
    static var previews: some View {
        UserCard(user: UserRealm())
    }
}
