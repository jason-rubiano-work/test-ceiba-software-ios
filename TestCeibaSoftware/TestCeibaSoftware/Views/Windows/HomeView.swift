//
//  HomeView.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import SwiftUI
import RealmSwift

struct HomeView: View {
    
    @EnvironmentObject var realmObserver: RealmObserver
    @State var searhUser: String = ""
    
    var body: some View {
        
        let textProxy = Binding<String> (
            get: {
                self.searhUser
            },
            set: {
                
                self.searhUser = $0.lowercased()
                
                realmObserver.filterUserRealmObserver(searhUser: self.searhUser)
            })
        
        NavigationView {
         
            VStack(spacing: 8) {
                
                SearchBar(searhText: textProxy)
                
                if !realmObserver.users.isEmpty {
                 
                    if !realmObserver.filterUser.isEmpty {
                        
                        ScrollView(.vertical, showsIndicators: true) {
                            
                            LazyVStack(spacing: 16) {
                                
                                ForEach(realmObserver.filterUser, id: \.id) { item in
                                    
                                    UserCard(user: item)
                                }
                                
                            }.padding(16)
                        }
                        
                    } else {
                     
                        ListEmptyView()
                    }
                    
                } else {
                    
                    LoadingAnimation()
                }
                
            }.navigationTitle("Buscar usuario")
                .disabled(realmObserver.users.isEmpty)
                .onAppear() {
                    
                    if realmObserver.users.isEmpty {
                     
                        realmObserver.downloadData()
                    }
                }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .environmentObject(RealmObserver())
    }
}
