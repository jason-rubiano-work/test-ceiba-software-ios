//
//  ListEmptyView.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import SwiftUI

struct ListEmptyView: View {
    var body: some View {
        
        VStack {
            
            Text("El usuario no existe")
                .font(.system(size: 28, weight: .bold))
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct ListEmptyView_Previews: PreviewProvider {
    static var previews: some View {
        ListEmptyView()
    }
}
