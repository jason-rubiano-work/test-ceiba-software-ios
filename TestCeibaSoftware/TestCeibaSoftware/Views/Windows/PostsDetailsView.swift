//
//  PostsDetailsView.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import SwiftUI

struct PostsDetailsView: View {
    
    @EnvironmentObject var realmObserver: RealmObserver
    
    @Binding var modal: Bool
    var userId: Int
    var name: String
    var email: String
    var phone: String
    
    var body: some View {
        
        VStack(spacing: 5) {
            
            HStack(alignment: .top, spacing: 4) {

                VStack(alignment: .leading, spacing: 5) {
                 
                    Text("Name: ")
                        .font(.system(size: 16, weight: .bold))
                    + Text(name)
                        .font(.system(size: 16, weight: .medium))
                    
                    Text("Email: ")
                        .font(.system(size: 16, weight: .bold))
                    + Text(email)
                        .font(.system(size: 16, weight: .medium))
                    
                    Text("Phone: ")
                        .font(.system(size: 16, weight: .bold))
                    + Text(phone)
                        .font(.system(size: 16, weight: .medium))
                    
                }.multilineTextAlignment(.leading)
                    .fixedSize(horizontal: false, vertical: true)
                
                Spacer()

                Button(action: {

                    modal.toggle()
                }) {

                    Image(systemName: "xmark")
                        .foregroundColor(.blue)
                        .imageScale(.medium)
                }

            }.padding([.horizontal, .top], 16)
            
            ScrollView(.vertical, showsIndicators: true) {
                
                LazyVStack(spacing: 16) {
                    
                    ForEach(realmObserver.posts.filter("userId = %@", userId), id: \.id) { item in
                        
                        PostCard(post: item)
                    }
                    
                }.padding(16)
            }
        }
    }
}

struct PostsDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        PostsDetailsView(modal: .constant(false), userId: 1, name: "", email: "", phone: "")
            .environmentObject(RealmObserver())
    }
}
