//
//  LocalGateway.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import Foundation
import RealmSwift

class LocalGateway {
    
    //MARK: CRUD users
    func writeUserRealm(user: UserDTO) {
        
        do {
            
            try realm.write {
                
                let geoRealm = GeoRealm()
                
                geoRealm.lat = user.address?.geo?.lat
                geoRealm.lng = user.address?.geo?.lng
                
                let addressRealm = AddressRealm()
                
                addressRealm.street = user.address?.street
                addressRealm.suite = user.address?.suite
                addressRealm.city = user.address?.city
                addressRealm.zipcode = user.address?.zipcode
                addressRealm.geo = geoRealm
                
                let companyRealm = CompanyRealm()
                
                companyRealm.name = user.company?.name
                companyRealm.catchPhrase = user.company?.catchPhrase
                companyRealm.bs = user.company?.bs
                
                let userRealm = UserRealm()
                
                userRealm.id = user.id ?? 0
                userRealm.name = user.name
                userRealm.username = user.username
                userRealm.email = user.email
                userRealm.address = addressRealm
                userRealm.phone = user.phone
                userRealm.website = user.website
                userRealm.company = companyRealm
                
                realm.add(userRealm)
            }
            
        } catch {
            print("Error writeUserRealm", error)
        }
    }
    
    func readUserRealm(id: Int) -> UserDTO? {
        
        let userRealm = realm.objects(UserRealm.self).filter("id = %@", id).first
       
        if userRealm != nil {
            
            return UserDTO(id: userRealm?.id, name: userRealm?.name, username: userRealm?.username, email: userRealm?.email, address: Address(street: userRealm?.address?.street, suite: userRealm?.address?.suite, city: userRealm?.address?.city, zipcode: userRealm?.address?.zipcode, geo: Geo(lat: userRealm?.address?.geo?.lat, lng: userRealm?.address?.geo?.lng)), phone: userRealm?.phone, website: userRealm?.website, company: Company(name: userRealm?.company?.name, catchPhrase: userRealm?.company?.catchPhrase, bs: userRealm?.company?.bs))
            
        } else {
            
            return nil
        }
    }
    
    func userRealmlastObjectID() -> Int {
        
        let lastObject = realm.objects(UserRealm.self).last
        
        if lastObject != nil {
         
            return lastObject?.id ?? 0
            
        } else {
            
            return 0
        }
    }
    
    func updateUserRealmItem(object: UserDTO) {
        
        do {
            
            try realm.write {
                
                guard let user = realm.objects(UserRealm.self).filter("id = %@", object.id ?? 0).first else {return}
                
                let geoRealm = GeoRealm()
                
                geoRealm.lat = user.address?.geo?.lat
                geoRealm.lng = user.address?.geo?.lng
                
                let addressRealm = AddressRealm()
                
                addressRealm.street = user.address?.street
                addressRealm.suite = user.address?.suite
                addressRealm.city = user.address?.city
                addressRealm.zipcode = user.address?.zipcode
                addressRealm.geo = geoRealm
                
                let companyRealm = CompanyRealm()
                
                companyRealm.name = user.company?.name
                companyRealm.catchPhrase = user.company?.catchPhrase
                companyRealm.bs = user.company?.bs
                
                let userRealm = UserRealm()
                
                userRealm.id = user.id
                userRealm.name = user.name
                userRealm.username = user.username
                userRealm.email = user.email
                userRealm.address = addressRealm
                userRealm.phone = user.phone
                userRealm.website = user.website
                userRealm.company = companyRealm
                
                realm.add(userRealm)
            }
            
        } catch {
            print("Error updateUserRealmItem", error)
        }
    }
    
    func deleteUserRealmItem(object: UserDTO) {
        
        do {
            
            try realm.write {
                
                guard let userRealm = realm.objects(UserRealm.self).filter("id = %@", object.id ?? 0).first else {return}
                
                realm.delete(userRealm)
            }
            
        } catch {
            print("Error deleteUserRealmItem", error)
        }
    }
    
    //MARK: CRUD posts
    func writePostsRealm(object: PostsDTO) {
        
        do {
            
            try realm.write {
                
                let postsRealm = PostsRealm()
                
                postsRealm.userId = object.userId ?? 0
                postsRealm.id = object.id ?? 0
                postsRealm.title = object.title
                postsRealm.body = object.body
                
                realm.add(postsRealm)
            }
            
        } catch {
            print("Error writePostsRealm", error)
        }
    }
    
    func readPostsRealm(id: Int) -> PostsDTO? {
        
        let postsRealm = realm.objects(PostsRealm.self).filter("id = %@", id).first
       
        if postsRealm != nil {
            
            return PostsDTO(userId: postsRealm?.userId ?? 0, id: postsRealm?.id ?? 0, title: postsRealm?.title, body: postsRealm?.body)
            
        } else {
            
            return nil
        }
    }
    
    func postsRealmlastObjectID() -> Int {
        
        let lastObject = realm.objects(PostsRealm.self).last
        
        if lastObject != nil {
         
            return lastObject?.id ?? 0
            
        } else {
            
            return 0
        }
    }
    
    func updatePostsRealm(object: PostsDTO) {
        
        do {
            
            try realm.write {
                
                guard let postsRealm = realm.objects(PostsRealm.self).filter("userId = %@", object.userId ?? 0).first else {return}
                
                postsRealm.userId = object.userId ?? 0
                postsRealm.id = object.id ?? 0
                postsRealm.title = object.title
                postsRealm.body = object.body
                
                realm.add(postsRealm)
            }
            
        } catch {
            print("Error updatePostsRealm", error)
        }
    }
    
    func deletePostsRealm(object: PostsDTO) {
        
        do {
            
            try realm.write {
                
                guard let postsRealm = realm.objects(PostsRealm.self).filter("userId = %@", object.userId ?? 0).first else {return}
                
                realm.delete(postsRealm)
            }
            
        } catch {
            print("Error deletePostsRealm", error)
        }
    }
}
