//
//  RemoteGateway.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import Foundation
import Alamofire

class RemoteGateway {
    
    let decoder = JSONDecoder()
    
    //MARK: Get users
    func getUsers(successCallback: @escaping ([UserDTO]) -> Void, errorCallback: @escaping (Any) -> Void, networkErrorCallback: @escaping (AFError) -> Void) {
        
        APIRequests.basicRequest(url: "users", body: nil, headers: [], method: .get, successCallback: { data in
            
            guard let json = data as? [NSDictionary] else {return}
            
            var users: [UserDTO] = []
            
            do {
                
                for user in json {
                 
                    let jsonData = try JSONSerialization.data(withJSONObject: user, options: [])
                    let decodedElement = try self.decoder.decode(UserDTO.self, from: jsonData)
                    users.append(decodedElement)
                }

                successCallback(users)
                
            } catch {
                print("error getUsers catch: ", error)
            }
            
        }, errorCallback: { error in
            
            errorCallback(error)
            
        }, networkErrorCallback: { error in
            
            networkErrorCallback(error)
        })
    }
    
    //MARK: Get posts
    func getPosts(successCallback: @escaping ([PostsDTO]) -> Void, errorCallback: @escaping (Any) -> Void, networkErrorCallback: @escaping (AFError) -> Void) {
        
        APIRequests.basicRequest(url: "posts", body: nil, headers: [], method: .get, successCallback: { data in
            
            guard let json = data as? [NSDictionary] else {return}
            
            var posts: [PostsDTO] = []
            
            do {
                
                for user in json {
                 
                    let jsonData = try JSONSerialization.data(withJSONObject: user, options: [])
                    let decodedElement = try self.decoder.decode(PostsDTO.self, from: jsonData)
                    posts.append(decodedElement)
                }

                successCallback(posts)
                
            } catch {
                print("error getPosts catch: ", error)
            }
            
        }, errorCallback: { error in
            
            errorCallback(error)
            
        }, networkErrorCallback: { error in
            
            networkErrorCallback(error)
        })
    }
}
