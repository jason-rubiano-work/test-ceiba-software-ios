//
//  APIRequests.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import Foundation
import Alamofire

struct Constants {
    
    static var API :String = "https://jsonplaceholder.typicode.com/"
}

//MARK: APIRequests
struct APIRequests {
    
    static func basicRequest (preLogin: Bool = false, url: String, body: [String: Any]?, headers: HTTPHeaders, method: HTTPMethod, successCallback: @escaping (Any) -> Void, errorCallback: @escaping (NSDictionary) -> Void, networkErrorCallback: @escaping (AFError) -> Void) {
        
        AF.request("\(Constants.API)\(url)", method: method, parameters: body, encoding: JSONEncoding.default, headers: headers)
            .responseData { response in
                
                switch response.result {
                    
                case .success(let data):
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: data)
                        
                        //guard let decode = json as? NSDictionary else {return}
                        let statusCode = response.response?.statusCode ?? 0
                        
                        switch statusCode {
                            
                        case 200..<300:
                            
                            successCallback(json)
                            
                        default:
                            
                            guard let decodeError = json as? NSDictionary else {return}
                            errorCallback(decodeError)
                        }
                        
                    } catch { print("erroMsg") }
                    
                case .failure(let error):
                    networkErrorCallback(error)
                }
            }
    }
}
