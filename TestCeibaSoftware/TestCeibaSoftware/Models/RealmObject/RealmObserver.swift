//
//  RealmObserver.swift
//  TestCeibaSoftware
//
//  Created by JASS on 7/04/22.
//

import Foundation
import RealmSwift

final class RealmObserver: ObservableObject {

    @Published var users: Results<UserRealm>
    @Published var posts: Results<PostsRealm>
    @Published var filterUser: [UserRealm] = []
    private var objectsRealm: NotificationToken? = nil
    
    init() {
        users = realm.objects(UserRealm.self)
        posts = realm.objects(PostsRealm.self)
        filterUserRealmObserver(searhUser: "")
        activateObjects()
    }
    
    private func activateObjects() {
        
        let objectsUser = realm.objects(UserRealm.self)
        objectsRealm = objectsUser.observe { _ in
            
            self.users = objectsUser
        }
        
        let objectsPost = realm.objects(PostsRealm.self)
        objectsRealm = objectsPost.observe { _ in
            
            self.posts = objectsPost
        }
    }
    
    //MARK: Filter data
    func filterUserRealmObserver(searhUser: String) {
        
        self.filterUser = users.filter({ ($0.name ?? "").lowercased().hasPrefix(searhUser) || searhUser.isEmpty })
    }
    
    //MARK: Download data
    func downloadData() {
        
        RemoteGateway().getUsers(successCallback: { data in
            
            for user in data {
                
                LocalGateway().writeUserRealm(user: user)
            }
            
            self.filterUserRealmObserver(searhUser: "")
            
            RemoteGateway().getPosts(successCallback: { data in
                
                for post in data {
                    
                    LocalGateway().writePostsRealm(object: post)
                }
                
            }, errorCallback: { error in }, networkErrorCallback: { error in })
            
        }, errorCallback: { error in }, networkErrorCallback: { error in })
    }
    
    //MARK: Update data
    func updateData() {
        
        RemoteGateway().getUsers(successCallback: { data in
            
            for user in data {
                
                if self.users.filter("id = %@", user.id ?? 0).isEmpty {
                    
                    LocalGateway().writeUserRealm(user: user)
                }
            }
            
            self.filterUserRealmObserver(searhUser: "")
            
            RemoteGateway().getPosts(successCallback: { data in
                
                for post in data {
                    
                    if self.posts.filter("id = %@", post.id ?? 0).isEmpty {
                     
                        LocalGateway().writePostsRealm(object: post)
                    }
                }
                
            }, errorCallback: { error in }, networkErrorCallback: { error in })
            
        }, errorCallback: { error in }, networkErrorCallback: { error in })
    }
}
