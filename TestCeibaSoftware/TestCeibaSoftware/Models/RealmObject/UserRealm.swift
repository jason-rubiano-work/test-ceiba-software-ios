//
//  UserRealm.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import Foundation
import RealmSwift

// Realm Model
class UserRealm: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String?
    @objc dynamic var username: String?
    @objc dynamic var email: String?
    @objc dynamic var address: AddressRealm?
    @objc dynamic var phone: String?
    @objc dynamic var website: String?
    @objc dynamic var company: CompanyRealm?
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class AddressRealm: Object {
    @objc dynamic var street: String?
    @objc dynamic var suite: String?
    @objc dynamic var city: String?
    @objc dynamic var zipcode: String?
    @objc dynamic var geo: GeoRealm?
    let userRealm = LinkingObjects(fromType: UserRealm.self, property: "address")
}

class GeoRealm: Object {
    @objc dynamic var lat: String?
    @objc dynamic var lng: String?
    let addressRealm = LinkingObjects(fromType: AddressRealm.self, property: "geo")
}

class CompanyRealm: Object {
    @objc dynamic var name: String?
    @objc dynamic var catchPhrase: String?
    @objc dynamic var bs: String?
    let userRealm = LinkingObjects(fromType: UserRealm.self, property: "company")
}
