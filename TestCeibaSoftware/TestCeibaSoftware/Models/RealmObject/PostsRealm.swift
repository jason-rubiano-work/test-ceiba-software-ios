//
//  PostsRealm.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import Foundation
import RealmSwift

// Realm Model
class PostsRealm: Object {
    @objc dynamic var userId: Int = 0
    @objc dynamic var id: Int = 0
    @objc dynamic var title: String?
    @objc dynamic var body: String?
}
