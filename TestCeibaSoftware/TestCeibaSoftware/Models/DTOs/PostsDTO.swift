//
//  PostsDTO.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import Foundation

struct PostsDTO: Decodable, Identifiable {
    var userId: Int?
    var id: Int?
    var title: String?
    var body: String?
}
