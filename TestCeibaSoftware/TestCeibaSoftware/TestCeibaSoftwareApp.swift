//
//  TestCeibaSoftwareApp.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import SwiftUI
import RealmSwift

let realm = try! Realm()

@main
struct TestCeibaSoftwareApp: SwiftUI.App {
    
    @StateObject var realmObserver = RealmObserver()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.realmConfiguration, Realm.Configuration(schemaVersion: 1))
                .environmentObject(realmObserver)
        }
    }
}
