//
//  ContentView.swift
//  TestCeibaSoftware
//
//  Created by JASS on 6/04/22.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        
        HomeView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(RealmObserver())
    }
}
