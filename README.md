# Test Ceiba Software iOS



## Descripción de negocio

Consiste en crear una aplicación móvil para iOS con el objetivo de listar usuarios y las publicaciones que éstos realizan.
Los usuarios y publicaciones se obtienen de unos web services tipo Rest que ya se encuentran construidos y usted solo deberá consumirlos.

## Reglas de negocio

1. Cuando se abra la aplicación verificar si los usuarios se encuentran almacenados de manera local, si es así solo se deben presentar; si no se encuentran almacenados localmente se debe consumir el Web Service, almacenar el resultado y presentarlos al usuario.
2. Al seleccionar un usuario debe mostrar nombre, teléfono, correo y listar sus publicaciones realizadas.
3. Se debe permitir filtrar los usuarios por nombre, cada que se escribe una letra debe ir mostrando los usuarios que su nombre corresponda con el filtro.

## Reglas de negocio

1. Utilizar método de su preferencia para el consumo de los servicios web.
2. La aplicación debe mostrar un diálogo de carga al consumir los servicios web.
3. Utilizar motor de base de datos de su preferencia para el almacenamiento local de éstos.
4. Utilizar Swift como lenguaje de programación si realiza la prueba en iOS Nativo.
5. Utilizar C# como lenguaje de programación si realiza la prueba en Xamarin iOS (Xamarin Native).
6. Interfaz de usuario similar a la que se muestra en esta presentación.

## Conceptos a evaluar

1. Buenas Prácticas de desarrollo móvil
2. Pruebas automatizadas y unitarias
3. Clean code
4. Arquitectura y separación de capas
5. Completitud de los requerimientos y exactamente de la manera indicada
